## <small>1.1.5 (2020-10-28)</small>

* 1.1.5 ([d3eadfd](https://gitlab.com/thomasjuster/promisedip/commit/d3eadfd))
* docs(changelog): generate ([ce01c66](https://gitlab.com/thomasjuster/promisedip/commit/ce01c66))
* docs(readme): add troubleshooting section ([794fc77](https://gitlab.com/thomasjuster/promisedip/commit/794fc77))



## <small>1.1.4 (2020-10-28)</small>

* 1.1.4 ([dd06926](https://gitlab.com/thomasjuster/promisedip/commit/dd06926))
* build(package.json): fix export map, my bad… ([f93af51](https://gitlab.com/thomasjuster/promisedip/commit/f93af51))
* docs(changelog): generate ([ffee67f](https://gitlab.com/thomasjuster/promisedip/commit/ffee67f))



## <small>1.1.3 (2020-10-27)</small>

* 1.1.3 ([da729d2](https://gitlab.com/thomasjuster/promisedip/commit/da729d2))
* chore(package): fill the new 'exports' package.json field ([8f48ba4](https://gitlab.com/thomasjuster/promisedip/commit/8f48ba4))
* chore(package): fix issues url ([2ff12fd](https://gitlab.com/thomasjuster/promisedip/commit/2ff12fd))
* docs(changelog): generate ([427a7ef](https://gitlab.com/thomasjuster/promisedip/commit/427a7ef))
* docs(readme): add a demo in codesandbox ([c027426](https://gitlab.com/thomasjuster/promisedip/commit/c027426))



## <small>1.1.2 (2020-10-27)</small>

* 1.1.2 ([b0a1fce](https://gitlab.com/thomasjuster/promisedip/commit/b0a1fce))
* docs(changelog): generate ([78af144](https://gitlab.com/thomasjuster/promisedip/commit/78af144))
* docs(readme): change absurd statement :shrug: ([9abb46f](https://gitlab.com/thomasjuster/promisedip/commit/9abb46f))
* docs(readme): remove parenthesis ([ffc416e](https://gitlab.com/thomasjuster/promisedip/commit/ffc416e))



## <small>1.1.1 (2020-10-27)</small>

* 1.1.1 ([7b1bae2](https://gitlab.com/thomasjuster/promisedip/commit/7b1bae2))
* docs(changelog): generate ([325b60c](https://gitlab.com/thomasjuster/promisedip/commit/325b60c))
* docs(readme): suggest another example ([fa18f48](https://gitlab.com/thomasjuster/promisedip/commit/fa18f48))
* docs(readme): update examples ([39411c5](https://gitlab.com/thomasjuster/promisedip/commit/39411c5))



## 1.1.0 (2020-10-27)

* 1.1.0 ([0c4f753](https://gitlab.com/thomasjuster/promisedip/commit/0c4f753))
* build(babel): remove babel since it was breaking the build ([ffe814f](https://gitlab.com/thomasjuster/promisedip/commit/ffe814f))
* docs(changelog): last fix to generate properly the changelog ([ec99c6c](https://gitlab.com/thomasjuster/promisedip/commit/ec99c6c))



## <small>1.0.4 (2020-10-26)</small>

* 1.0.4 ([ba77384](https://gitlab.com/thomasjuster/promisedip/commit/ba77384))
* docs(changelog): update changelog ([a1a42db](https://gitlab.com/thomasjuster/promisedip/commit/a1a42db))
* docs(readme): add a word on typescript ([50549a5](https://gitlab.com/thomasjuster/promisedip/commit/50549a5))
* docs(readme): be more precise, you can never be more precise ([1db77c3](https://gitlab.com/thomasjuster/promisedip/commit/1db77c3))
* docs(readme): change badges order for readability ([5d71847](https://gitlab.com/thomasjuster/promisedip/commit/5d71847))



## <small>1.0.3 (2020-10-26)</small>

* 1.0.3 ([dfe61b2](https://gitlab.com/thomasjuster/promisedip/commit/dfe61b2))
* docs(readme): add badges ([44a05ab](https://gitlab.com/thomasjuster/promisedip/commit/44a05ab))
* test(map method): add test to obtain 100% coverage ([cf54926](https://gitlab.com/thomasjuster/promisedip/commit/cf54926))
* ci(coverage): enable code coverage ([2da4686](https://gitlab.com/thomasjuster/promisedip/commit/2da4686))
* ci(gitlab): set up ci ([94f53a5](https://gitlab.com/thomasjuster/promisedip/commit/94f53a5))
* style(lint): setup eslint properly and fix source files ([b63862d](https://gitlab.com/thomasjuster/promisedip/commit/b63862d))
* chore(changelog): use commitizen as a commit & changelog tool ([5dc3184](https://gitlab.com/thomasjuster/promisedip/commit/5dc3184))



## <small>1.0.2 (2020-10-26)</small>

* 1.0.2 ([3a3a1db](https://gitlab.com/thomasjuster/promisedip/commit/3a3a1db))
* remove comment ([0b8efcc](https://gitlab.com/thomasjuster/promisedip/commit/0b8efcc))
* chore(build): enhance rollup config and minification ([dffdeee](https://gitlab.com/thomasjuster/promisedip/commit/dffdeee))



## <small>1.0.1 (2020-10-23)</small>

* 1.0.1 ([a246a92](https://gitlab.com/thomasjuster/promisedip/commit/a246a92))
* Add LICENSE ([144048d](https://gitlab.com/thomasjuster/promisedip/commit/144048d))
* bootstrap lib ([b96cdbd](https://gitlab.com/thomasjuster/promisedip/commit/b96cdbd))
* bootstrap lib ([c62ec91](https://gitlab.com/thomasjuster/promisedip/commit/c62ec91))
* Initial commit ([6b3d9b6](https://gitlab.com/thomasjuster/promisedip/commit/6b3d9b6))
* chore: rename package because npm is grumpy ([ded96b0](https://gitlab.com/thomasjuster/promisedip/commit/ded96b0))
* chore(package): change repo-based urls ([c8d69cb](https://gitlab.com/thomasjuster/promisedip/commit/c8d69cb))
* doc: add home page url, repo and fix readme ([6ff1e30](https://gitlab.com/thomasjuster/promisedip/commit/6ff1e30))



