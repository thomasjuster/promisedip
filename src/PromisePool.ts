type PromiseCreator<T> = () => Promise<T>;
type IsConsumed = boolean;
type SettledListener<T> = (creator: PromiseCreator<T>, promise: Promise<T>) => IsConsumed;

interface Options {
  concurrency: number;
}

export default class PromisePool {
  private _stack: Array<PromiseCreator<unknown>>;
  private _pending: number;
  private _concurrency: number;
  private _listeners: Array<SettledListener<unknown>>;

  constructor ({ concurrency }: Options) {
    const mustBe = (msg: string) => {
      throw new Error(`concurrency must be ${msg}`)
    }
    if (!Number.isFinite(concurrency)) mustBe('finite')
    if (!Number.isInteger(concurrency)) mustBe('an integer')
    if (concurrency < 1) mustBe('greater than 0')
    this._stack = []
    this._pending = 0
    this._concurrency = concurrency
    this._listeners = []
  }

  public static async map<T, U> (
    array: T[] | readonly T[],
    mapper: (item: T, index: number, array: T[] | readonly T[]) => Promise<U>,
    options: Options,
  ): Promise<U[]> {
    const pool = new PromisePool(options)
    return Promise.all([...array].map(async (item, index, arr) => pool.resolve(async () => mapper(item, index, arr))))
  }

  public async resolve<T> (promiseCreator: PromiseCreator<T>): Promise<T> {
    const promise = this._promiseSettled<T>(promiseCreator)
    this._stack.push(promiseCreator)
    if (this._stack.length === 1) this._process()
    return promise
  }

  private _notify (creator: PromiseCreator<unknown>, promise: Promise<unknown>): void {
    const listenersToRemove: Array<SettledListener<unknown>> = []
    this._listeners.forEach((listener) => {
      const isConsumed = listener(creator, promise)
      isConsumed && listenersToRemove.push(listener)
    })
    this._listeners = this._listeners.filter((listener) => !listenersToRemove.includes(listener))
  }

  private _listen<T> (listener: SettledListener<T>): void {
    this._listeners.push(listener as SettledListener<unknown>)
  }

  private async _promiseSettled<T> (promiseCreator: PromiseCreator<T>): Promise<T> {
    return new Promise((resolve) => {
      this._listen((creator, promise) => {
        if (creator !== promiseCreator) return false
        resolve(promise as Promise<T>)
        return true
      })
    })
  }

  private async _anySettled (): Promise<void> {
    return new Promise((resolve) => {
      this._listen(() => {
        resolve()
        return true
      })
    })
  }

  private async _process (): Promise<void> {
    while (this._stack.length > 0) {
      while (this._pending >= this._concurrency) {
        await this._anySettled()
      }
      const creator = this._stack.shift() as PromiseCreator<unknown>

      const promise = creator()
      this._pending += 1

      const onPromiseSettled = (): void => {
        this._notify(creator, promise)
        this._pending -= 1
      }
      promise
        .then(onPromiseSettled, onPromiseSettled)
        .catch(onPromiseSettled)
    }
  }
}
