import PromisePool from './PromisePool'

async function delay (ms: number): Promise<void> {
  return new Promise((resolve) => setTimeout(() => resolve(), ms))
}
async function resolveAfter (value: string, ms: number): Promise<string> {
  await delay(ms)
  return value
}
async function rejectAfter (message: string, ms: number): Promise<string> {
  await delay(ms)
  throw new Error(message)
}

describe('PromisePool', () => {
  describe('parameters', () => {
    describe('concurrency', () => {
      it('should fail when not an integer', async () => {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore I'm testing specifically an outside typescript scenario
        expect(() => new PromisePool({ concurrency: 'toto' })).toThrow()
        expect(() => new PromisePool({ concurrency: 2.2 })).toThrow()
      })

      it('should fail when lower than 1', () => {
        expect(() => new PromisePool({ concurrency: 0 })).toThrow()
      })

      it('should fail when infinite', () => {
        expect(() => new PromisePool({ concurrency: Infinity })).toThrow()
      })
    })
  })

  describe('basic usage', () => {
    it('should work with resolved AND rejected promises', async () => {
      expect.assertions(1)
      const pool = new PromisePool({ concurrency: 2 })
      try {
        await Promise.all([
          pool.resolve(async () => resolveAfter('john', 30)),
          pool.resolve(async () => rejectAfter('Oopsie', 20)),
          pool.resolve(async () => resolveAfter('paul', 50)),
        ])
      } catch (error) {
        expect(error.message).toBe('Oopsie')
      }
    })

    it('should work with resolved promises', async () => {
      const pool = new PromisePool({ concurrency: 2 })
      const [john, paul, george, ringo] = await Promise.all([
        pool.resolve(async () => resolveAfter('john', 20)),
        pool.resolve(async () => resolveAfter('paul', 20)),
        pool.resolve(async () => resolveAfter('george', 15)),
        pool.resolve(async () => resolveAfter('ringo', 10)),
      ])
      expect(john).toBe('john')
      expect(paul).toBe('paul')
      expect(george).toBe('george')
      expect(ringo).toBe('ringo')
    })
  })

  describe('static methods', () => {
    describe('map()', () => {
      const beatles: string[] = ['john', 'paul', 'george', 'ringo']
      it('should work with resolved AND rejected promises', async () => {
        expect.assertions(1)

        const message = 'Aaaah'
        try {
          await PromisePool.map(beatles, async (beatle) => {
            await delay(42)
            if (beatle === 'george') throw new Error(message)
            return beatle
          }, { concurrency: 2 })
        } catch (error) {
          expect(error.message).toBe(message)
        }
      })

      it('should work with resolved promises', async () => {
        const result = await PromisePool.map(beatles, async (beatle) => {
          await delay(42)
          return `Beatle: ${beatle}`
        }, { concurrency: 2 })
        expect(result).toEqual(beatles.map((beatle) => `Beatle: ${beatle}`))
      })
    })
  })
})
