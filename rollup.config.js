import typescript from '@rollup/plugin-typescript'
import resolve from '@rollup/plugin-node-resolve'
import { terser } from 'rollup-plugin-terser'
import pkg from './package.json'

const production = process.env.NODE_ENV === 'production'

export default {
  input: './src/PromisePool.ts',
  output: [
    { file: pkg.module, format: 'esm', sourcemap: !production },
    { file: pkg.main, format: 'cjs', exports: 'default', sourcemap: !production },
    { file: pkg.browser, format: 'umd', name: 'PromisePool', sourcemap: !production },
  ],
  plugins: [
    resolve({ browser: true }),
    typescript({ sourceMap: !production, inlineSources: !production }),
    production && terser({ mangle: { properties: { regex: /^_/ } } }),
  ],
}
