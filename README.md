[![pipeline status](https://gitlab.com/thomasjuster/promisedip/badges/master/pipeline.svg)](https://gitlab.com/thomasjuster/promisedip/-/commits/master)
[![coverage report](https://gitlab.com/thomasjuster/promisedip/badges/master/coverage.svg)](https://gitlab.com/thomasjuster/promisedip/-/commits/master)
[![npm version](https://badge.fury.io/js/promisedip.svg)](https://badge.fury.io/js/promisedip)
[![Downloads](https://img.shields.io/npm/dm/promisedip.svg)](https://www.npmjs.com/package/promisedip)
[![js-standard-style](https://img.shields.io/badge/code%20style-standard-brightgreen.svg)](http://standardjs.com)
[![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg)](http://commitizen.github.io/cz-cli/)

## NOTE: This lib exposes next-gen code ; this means that you might need polyfills in old environments (e.g.: node < 7.6 or IE).

It ships with:
- [async/await syntax](https://caniuse.com/async-functions): not supported in IE and NodeJS < 7.6.0 ([MDN](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Instructions/async_function))
- [ES6 classes](https://caniuse.com/es6-class): not supported in IE and NodeJS < 6 ([MDN](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Classes))
- [class static method](https://caniuse.com/mdn-javascript_classes_static) not supported in IE and NodeJS < 6 ([MDN](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Classes/static))
- [Number.isFinite](https://caniuse.com/mdn-javascript_builtins_number_isfinite) not supported in IE and NodeJS < … 0.10 (wow that's oold) ([MDN](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Number/isFinite))
- [Number.isInteger](https://caniuse.com/mdn-javascript_builtins_number_isinteger) not supported in IE and NodeJS < … 0.12 (wow that's very old too) ([MDN](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Number/isInteger))

# Installation

## NPM

```bash
npm i -S promisedip
yarn add promisedip
```

## JavaScript module

```html
<script type="module">
  import PromisePool from 'https://unpkg.com/promisedip@1.0.2/lib/PromisePool.esm.js';
  // …
</script>
```

# Demo

I made a [codesandox](https://codesandbox.io/s/promisedip-vkpyo?file=/demo.js) to visualize the promise pooling (in the console)

# API

*NB*: *Everything is TypeScript-friendly*

## `new PromisePool(options)`

**Usage:**
```ts
new PromisePool(options: { concurrency: number })
```

**Example**
```ts
import PromisePool from 'promisedip'

const pool = new PromisePool({ concurrency: 2 })

async function doStuff (): Promise<void> {
  const [
    examplePage,
    wordscapePage,
    wiki,
  ] = await Promise.all([
    // pool will execute the following promises in the correct order respecting the concurrency option provided
    pool.resolve(() => fetch('https://example.com/')),
    pool.resolve(() => fetch('https://wordscape.com/')),
    pool.resolve(async () => {
      const response = await fetch('https://en.wikipedia.org/')
      // … do anything else you need
      return response
    }),
    // and so on…
  ])
}
```

## `PromisePool.map()`

**Usage**
```ts
PromisePool.map<T, U>(
  array: Array<T>,
  mapper: (item: T, index: number, array: Array<T>) => Promise<U>,
  options: { concurrency: number },
): Promise<U[]>
```

**Example**
```ts
import PromisePool from 'promisedip'

const pages = [
  'https://example.com/',
  'https://wordscape.com/',
  'https://en.wikipedia.org/',
]

async function main (): Promise<void> {
  const results: string[] = await PromisePool.map(
    pages,
    async (page) => {
      const response = await fetch(page)
      return response.text()
    },
    { concurrency: 2 },
  )
}
```

# Troubleshooting

- `PromisePool.map(readOnlyArray, …)` does not work with a readonly array
  - Use `PromisePool.map([...readOnlyArray], …)`
- The lib does not seem to work with Node, or TypeScript, or anywhere except the browser
  - TL;DR: install a version >= 1.1.4
  - Explanation: I made successive mistakes in the build process, I didn't master fully [exports map](https://github.com/jkrems/proposal-pkg-exports/) nor rollupjs for building UMD, CommonJS **AND** ESModule outputs. Now everything is fine.

# Why

This package is clearly not the only one to perform promise pooling. So why "yet another …" ?

Because …
- I wanted a **dependency-free package** that I could use for frontend projects
- Since I intend to use it on frontend apps, I wanted it to be **extremely lightweight**: the JavaScript module build is **less than 1kB**
- I wanted a pooler that I could use at **different scopes**, from very global to very local
- I wanted to be able to **add promises** in the stack **on the fly**
- I wanted a **proper naming**
- I wanted **proper typings**
- I wanted **JavaScript modules**
